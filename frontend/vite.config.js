import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue()
  ],
  server: {
    port: 3000,
    open: true,
    proxy: {
      '^/api/.*': 'http://localhost:3030',
      '^/authentication': 'http://localhost:3030',
    }
 },
})
