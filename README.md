
# Créer une base de données sqlite 'dev.sqlite3' avec une table 'users'
```
$ cd backend
$ sqlite3 dev.sqlite3
> CREATE TABLE users (id integer primary key autoincrement, email text, password text);
```

# Lancer le backend
```
npm run dev
```

# Lancer le frontend
```
npm run dev
```
