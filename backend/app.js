const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const knex = require('knex')
const knexService = require('feathers-knex')
const configuration = require('@feathersjs/configuration')

const { AuthenticationService, JWTStrategy } = require('@feathersjs/authentication');
const { LocalStrategy } = require('@feathersjs/authentication-local');
const { authenticate } = require('@feathersjs/authentication').hooks
const { hashPassword, protect } = require('@feathersjs/authentication-local').hooks

// Create an Express compatible Feathers application
const app = express(feathers())

// read config/default.js file -> see keys: 'port', 'authentication'
app.configure(configuration())

// Turn on JSON parser for REST services
app.use(express.json())
// Set up HTTP transport
app.configure(express.rest())

// create '/api/users' rest service over a knex/sqlite database
const database = knex({
   client: 'sqlite3',
   connection: { filename: './dev.sqlite3' }
})
const usersService = knexService({ Model: database, name: 'users' })
app.use('/api/users', usersService)

// add hooks to '/api/users' service
app.service('/api/users').hooks({
   before: {
      get: [authenticate('jwt')],
      find: [authenticate('jwt')],
      update: [authenticate('jwt')],
      patch: [authenticate('jwt')],
      remove: [authenticate('jwt')],
      create: [hashPassword('password')],
   },
   after: {
      all: [protect('password')],
   },
})

// Register authentication
const authenticationService = new AuthenticationService(app)
authenticationService.register('jwt', new JWTStrategy())
authenticationService.register('local', new LocalStrategy())
app.use('/authentication', authenticationService)

// Start server on port 3030
app.listen(app.get('port'), () => console.log(`listening on port ${app.get('port')}`))
