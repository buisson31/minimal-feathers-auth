module.exports = {
   port: 3030,

   authentication: {
      entity: "user",
      service: "/api/users",
      secret: "A3lxcjmVD0af6Hjd1fksZyzFGtE=",
      authStrategies: [
         "jwt",
         "local"
      ],
      jwtOptions: {},
      // jwtOptions: {
      //    header: {
      //       typ: "access"
      //    },
      //    audience: "https://mydomain.com",
      //    issuer: "feathers",
      //    algorithm: "HS256",
      //    expiresIn: "1h"
      // },
      local: {
         usernameField: "email",
         passwordField: "password"
      }
   },
}
